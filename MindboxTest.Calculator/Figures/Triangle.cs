﻿namespace MindboxTest.Calculator.Figures;

public class Triangle: AbstractFigure
{
    #region FieldsAndCtor

    /// <summary>
    /// Первая сторона
    /// </summary>
    public double FirstSide { get; }
    
    /// <summary>
    /// Вторая сторона
    /// </summary>
    public double SecondSide { get; }
    
    /// <summary>
    /// Третья сторона
    /// </summary>
    public double ThirdSide { get; }
    
    /// <summary>
    /// Прямоугольный ли треугольник
    /// </summary>
    public bool IsRightTriangle { get; }
    
    private readonly double _perimeter;

    /// <summary>
    /// Конструктор класса треугольник 
    /// </summary>
    /// <param name="firstSide">Первая сторона</param>
    /// <param name="secondSide">Вторая сторона</param>
    /// <param name="thirdSide">Третья сторона</param>
    /// <param name="symbolsAfterComma"></param>
    /// <exception cref="ArgumentException">
    /// </exception>
    public Triangle(double firstSide, double secondSide, double thirdSide, int symbolsAfterComma = 0) : base(symbolsAfterComma)
    {
        ValidateSidesLength(firstSide, secondSide, thirdSide);

        double maxSide = firstSide, b = secondSide, c = thirdSide;
        if (b - maxSide > double.Epsilon) (maxSide, b) = (b, maxSide);
        if (c - maxSide > double.Epsilon) (maxSide, c) = (c, maxSide);
        
        _perimeter = firstSide + secondSide + thirdSide;
        
        ValidateGreatestSide(maxSide);
        
        IsRightTriangle = Math.Abs(Math.Pow(maxSide, 2) - Math.Pow(b, 2) - Math.Pow(c, 2)) < double.Epsilon;

        FirstSide = firstSide;
        SecondSide = secondSide;
        ThirdSide = thirdSide;
    }

    #endregion
    
    #region PublicMethods
    
    public override double GetArea()
    {
        var semiPerimeter = _perimeter / 2;
        return Math.Round(
            Math.Sqrt(semiPerimeter * (semiPerimeter - FirstSide) * 
                      (semiPerimeter - SecondSide) * (semiPerimeter - ThirdSide)),
            SymbolsAfterComma
        );
    }
    
    #endregion

    #region PrivateMethods

    private void ValidateSidesLength(double firstSide, double secondSide, double thirdSide)
    {
        if (firstSide < double.Epsilon)
            throw new ArgumentException($"Первая сторона должна быть больше чем {double.Epsilon}", nameof(firstSide));

        if (secondSide < double.Epsilon)
            throw new ArgumentException($"Вторая сторона должна быть больше чем {double.Epsilon}", nameof(secondSide));

        if (thirdSide < double.Epsilon)
            throw new ArgumentException($"Третья сторона должна быть больше чем {double.Epsilon}", nameof(thirdSide));
    }

    private void ValidateGreatestSide(double maxSide)
    {
        if (_perimeter - 2 * maxSide < double.Epsilon)
            throw new ArgumentException("Наибольшая сторона должна быть меньше чем сумма 2 других сторон");
    }

    #endregion
}