﻿namespace MindboxTest.Calculator.Figures;

public class Circle: AbstractFigure
{
    #region FieldsAndCtor
    
    /// <summary>
    /// Радиус круга
    /// </summary>
    public double Radius { get; }

    /// <summary>
    /// Конструктор класса круг
    /// </summary>
    /// <param name="radius">Радиус круга</param>
    /// <param name="symbolsAfterComma"></param>
    /// <exception cref="ArgumentException"></exception>
    public Circle(double radius, int symbolsAfterComma = 0) : base(symbolsAfterComma)
    {
        ValidateInputData(radius, symbolsAfterComma);
        
        Radius = radius;
        SymbolsAfterComma = symbolsAfterComma;
    }
    
    #endregion

    #region PublicMethods
    
    public override double GetArea() => Math.Round(Math.PI * Math.Pow(Radius, 2), SymbolsAfterComma);

    #endregion

    #region PrivateMethods

    private void ValidateInputData(double radius, int symbolsAfterComma)
    {
        if (radius < double.Epsilon)
            throw new ArgumentException($"Радиус должен быть больше чем {double.Epsilon}", nameof(radius));
    }

    #endregion
}