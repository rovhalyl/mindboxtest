﻿namespace MindboxTest.Calculator.Figures;

public abstract class AbstractFigure
{
    public int SymbolsAfterComma { get; set; }

    protected AbstractFigure(int symbolsAfterComma)
    {
        ValidateSymbolsAfterComma(symbolsAfterComma);
        
        SymbolsAfterComma = symbolsAfterComma;
    }
    
    #region PublicMethods
    
    public abstract double GetArea();
    
    #endregion

    #region PrivateMethods

    private void ValidateSymbolsAfterComma(int symbolsAfterComma)
    {
        if (symbolsAfterComma is < 0 or > 15)
            throw new ArgumentException(
                "Количество символов после запятой должно быть от 1 до 15 ",
                nameof(symbolsAfterComma)
            );
    }

    #endregion
}