﻿namespace MindboxTest.Calculator.Common.Interfaces;

public interface IFigure
{
    /// <summary>
    /// Метод для расчета площади фигуры
    /// </summary>
    /// <returns>Area</returns>
    public double GetArea();
}